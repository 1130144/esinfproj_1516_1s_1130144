/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESINFPROJ;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Telmo
 */
public class DecoderTest {

    public DecoderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of openFile method, of class Decoder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testOpenFile() throws Exception {
        System.out.println("openFile");
        String fileName = "D:\\Faculdade\\project.dat";
        List<String> expResult = new ArrayList<>();
        List<String> result = Decoder.openFile(fileName);

        expResult.add(0, "A,VCA,High level analysis,1,week,30,112");
        expResult.add(1, "B,FCA,Order Hardware platform,4,week,2500");
        expResult.add(2, "C,FCA,Installation and commissioning of hardware,2,week,1250,B");
        expResult.add(3, "D,VCA,Detailed analysis of core modules,3,week,30,162,A");
        expResult.add(4, "E,VCA,Detailed analysis of supporting modules,2,week,30,108,D");
        expResult.add(5, "F,VCA,Programming of core modules,4,week,20,108,C,D");
        expResult.add(6, "G,VCA,Programming of supporting modules ,3,week,20,54,E,F");
        expResult.add(7, "H,VCA,Quality assurance of core modules,2,week,30,54,F");
        expResult.add(8, "I,VCA,Quality assurance of supporting modules,1,week,30,27,G");
        expResult.add(9, "J,FCA,Application Manual,1,week,550,G");
        expResult.add(10, "K,FCA,User Manual,1,week,750,G");
        expResult.add(11, "L,FCA,Core and supporting module training,2,week,1500,H,I,K");

        assertEquals("Os dados de entrada não são iguais.", expResult, result);
    }

    /**
     * Test of line method, of class Decoder.
     */
    @Test
    public void testLine() {
        System.out.println("line");
        List<String> str = null;
        List<Activities> expResult = null;
        List<Activities> result = Decoder.line(str);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
