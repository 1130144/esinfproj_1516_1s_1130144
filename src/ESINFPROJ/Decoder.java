package ESINFPROJ;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Telmo Marques, Nº 1130144
 */
public class Decoder {

    /**
     * Method that opens a file and move each of the lines for a list.
     *
     * @param fileName the file to open.
     * @return the List with the content of the file.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static List<String> openFile(String fileName)
            throws FileNotFoundException, IOException {

        List<String> file = new ArrayList();

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            file.add(line); //adiciona linha a lista
            line = br.readLine();
        }
        br.close();

        return file;
    }

    /**
     * Method that places each line ordered in the list of activities.
     *
     * @param str The List with the content of the file.
     * @return the register of the activities.
     */
    public static List<Activities> line(List<String> str) {

        //creates a structure for the list of objects
        List<Activities> Record = new ArrayList<Activities>();

        for (int i = 0; i < str.size(); i++) {

            List<String> Data = Arrays.asList(str.get(i).split(",")); //divide a linha em varias strings

            // Checks if the activity is from the type VCA
            if (Data.get(1).toUpperCase().contains("VCA")) {
                //test if the size of the line is equal to 7
                if (Data.size() == 7) {
                    Activities VCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), Data.get(5), Data.get(6), "-", "-", "-", "-");
                    Record.add(VCA);
                }
                //test if the size of the line is equal to 8
                if (Data.size() == 8) {
                    Activities VCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), Data.get(5), Data.get(6), Data.get(7), "-", "-", "-");
                    Record.add(VCA);
                }
                //test if the size of the line is equal to 9
                if (Data.size() == 9) {
                    Activities VCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), Data.get(5), Data.get(6), Data.get(7), Data.get(8), "-", "-");
                    Record.add(VCA);
                }
                //test if the size of the line is equal to 10
                if (Data.size() == 10) {
                    Activities VCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), Data.get(5), Data.get(6), Data.get(7), Data.get(8), Data.get(9), "-");
                    Record.add(VCA);
                }

            } // Checks if the activity is from the type FCA
            else if (Data.get(1).toUpperCase().contains("FCA")) {
                //test if the size of the line is equal to 6
                if (Data.size() == 6) {
                    Activities FCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), "", "", Data.get(5), "-", "-", "-");
                    Record.add(FCA);
                }
                //test if the size of the line is equal to 7
                if (Data.size() == 7) {
                    Activities FCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), "-", "-", Data.get(5), Data.get(6), "-", "-");
                    Record.add(FCA);
                }
                //test if the size of the line is equal to 8
                if (Data.size() == 8) {
                    Activities FCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), "-", "-", Data.get(5), Data.get(6), Data.get(7), "-");
                    Record.add(FCA);
                }
                //test if the size of the line is equal to 9
                if (Data.size() == 9) {
                    Activities FCA = new Activities(Data.get(0), Data.get(1), Data.get(2), Data.get(3),
                            Data.get(4), "-", "-", Data.get(5), Data.get(6), Data.get(7), Data.get(8));
                    Record.add(FCA);
                }
            }
        }
        return Record;
    }
}
