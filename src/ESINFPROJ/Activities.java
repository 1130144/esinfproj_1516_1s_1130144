package ESINFPROJ;

import java.io.Serializable;

/**
 *
 * @author Telmo Marques, Nº 1130144
 */
public class Activities implements Serializable {

    private String activKey;
    private String activType;
    private String descr;
    private String duration;
    private String duration_unit;
    private String cost_time;
    private String total_time;
    private String tot_cost;
    private String actkeyprev1;
    private String actkeyprev2;
    private String actkeyprev3;

    /**
     * Creates an instance of an Activity.
     *
     * @param activKey the activity key.
     * @param activType the activity type.
     * @param descr the descrition of the activity.
     * @param duration the duration of the activity.
     * @param duration_unit the unit duration of the activity.
     * @param cost_time the cost/time of the activity.
     * @param total_time the total time of the activity.
     * @param tot_cost the total cost of the activity.
     * @param actkeyprev1 the previous activity 1.
     * @param actkeyprev2 the previous activity 2.
     * @param actkeyprev3 the previous activity 3.
     */
    public Activities(String activKey, String activType, String descr,
            String duration, String duration_unit, String cost_time,
            String total_time, String tot_cost, String actkeyprev1,
            String actkeyprev2, String actkeyprev3) {
        this.activKey = activKey;
        this.activType = activType;
        this.descr = descr;
        this.duration = duration;
        this.duration_unit = duration_unit;
        this.cost_time = cost_time;
        this.total_time = total_time;
        this.tot_cost = tot_cost;
        this.actkeyprev1 = actkeyprev1;
        this.actkeyprev2 = actkeyprev2;
        this.actkeyprev3 = actkeyprev3;
    }

    public String getActivKey() {
        return activKey;
    }

    public void setActivKey(String activKey) {
        this.activKey = activKey;
    }

    public String getActivType() {
        return activType;
    }

    public void setActivType(String activType) {
        this.activType = activType;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration_unit() {
        return duration_unit;
    }

    public void setDuration_unit(String duration_unit) {
        this.duration_unit = duration_unit;
    }

    public String getCost_time() {
        return cost_time;
    }

    public void setCost_time(String cost_time) {
        this.cost_time = cost_time;
    }

    public String getTotal_time() {
        return total_time;
    }

    public void setTotal_time(String total_time) {
        this.total_time = total_time;
    }

    public String getTot_cost() {
        return tot_cost;
    }

    public void setTot_cost(String tot_cost) {
        this.tot_cost = tot_cost;
    }

    public String getActkeyprev1() {
        return actkeyprev1;
    }

    public void setActkeyprev1(String actkeyprev1) {
        this.actkeyprev1 = actkeyprev1;
    }

    public String getActkeyprev2() {
        return actkeyprev2;
    }

    public void setActkeyprev2(String actkeyprev2) {
        this.actkeyprev2 = actkeyprev2;
    }

    public String getActkeyprev3() {
        return actkeyprev3;
    }

    public void setActkeyprev3(String actkeyprev3) {
        this.actkeyprev3 = actkeyprev3;
    }

    @Override
    public String toString() {
        return "Activity Key " + this.activKey + ", Type of Activity " + this.activType
                + " with the description " + this.descr + " and the duration of " + this.duration
                + " " + this.duration_unit + " with the Cost/Time " + this.cost_time
                + " and a total activity time of " + this.total_time + ". Total activity Cost of "
                + this.tot_cost + ". Previous activities " + this.actkeyprev1
                + ",  " + this.actkeyprev2 + " and " + this.actkeyprev3 + ".\n";
    }
}
